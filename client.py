import argparse
import requests
import json
import os
import pandas as pd

from stairwai_utils import generate_model_json


def cli():
    parser = argparse.ArgumentParser(description="Bonseyes Benchmark-as-a-Service client tool")
    parser.add_argument(
        '--model-path', 
        '-mo', 
        required=True, 
        type=str, 
        help='Path to model file'
    )
    parser.add_argument(
        '--platform',
        '-pl',
        required=True,
        type=str,
        choices=['x86_64', 'jetson', 'rpi'],
        help='Choose one or more engines: x86_64 | jetson | rpi',
    )
    parser.add_argument(
        '--profile',
        '-pr',
        required=True,
        type=str,
        choices=[
            'cpu',
            'cuda11.4_tensorrt8.0',
            'cuda11.5_tensorrt8.2',
            'jetpack4.6',
            'arm64v8'
        ],
        help='Available environments: [cpu, cuda10.2_tensorrt7.0, cuda11.2_tensorrt7.2_rtx3070, cuda11.4_tensorrt8.0, cuda11.5_tensorrt8.2, jetpack4.4, jetpack4.6, arm64v8]',
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=True,
        type=str,
        choices=['tensorflow', 'onnxruntime', 'tensorrt', 'tflite', 'lpdnn'],
        help='Choose one or more engines: tensorflow | onnxruntime | tensorrt | tflite | lpdnn',
    )
    parser.add_argument(
        '--url',
        '-u',
        required=True,
        default='http://localhost',
        type=str,
        help='Dispatch server URL'
    )
    parser.add_argument(
        '--port',
        '-p',
        required=False,
        type=int,
        help='Dispatch server port'
    )
    parser.add_argument(
        '--device',
        '-de',
        required=False,
        default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )

    return parser


def main():
    args = cli().parse_args()

    platform = args.platform
    profile = args.profile
    engine = args.engine
    device = args.device
    cpu_num = args.cpu_num
    thread_num = args.thread_num
    port = f':{args.port}' if args.port is not None else f''

    dispatch_url = f'{args.url}{port}/bonseyes_baas/' + \
                    f'{platform}/{profile}/{engine}?device={device}'
    if cpu_num is not None:
        dispatch_url += f'&cpu_num={cpu_num}'
    if thread_num is not None:
        dispatch_url += f'&thread_num={thread_num}'

    ret = requests.post(
        dispatch_url, 
        files={'model': open(args.model_path, 'rb')}, 
        timeout=None,
        verify=False
    )

    if ret.status_code == 200:

        model_name, extension = os.path.splitext(args.model_path.split('/')[-1])
        
        csv_path = f'eval_results/{model_name}-{platform}.csv'
        results = ret.json()
        results['MODEL'] = model_name
        results['PLATFORM'] = platform
        df = pd.DataFrame([results])
        df.to_csv(csv_path, index=False)

        json_path = f'eval_results/{model_name}-{platform}.json'
        with open(json_path, 'w') as f:
            json.dump(generate_model_json(model_name, platform), f, indent=4)

        print(json.dumps(ret.json(), indent=4))

    else:
        
        print(ret.text)


if __name__ == '__main__':
    main()
