# Benchmark-as-a-Service (BaaS)

The BaaS (Benchmark as a Service) is a powerful service layer built upon the Bonseyes tools, enabling users to benchmark various AI models. By doing so, the BaaS expands the scope and enhances the Bonseyes tools' usability while facilitating interoperability with the AI-on-demand platform. With BaaS, users gain the ability to evaluate the performance of their AI models efficiently and effectively. 

The figure below illustrates the interfaces between the AI-on-demand Platform, the AI artifacts from AI4EU, and the BaaS:

![BaaS](doc/BaaS_design.png)

The dockerized service allows the following functions:
- Transfer of AI models from the AI-on-demand Platform to the BaaS for benchmarking by providing CLI or HTTP interfaces. 
- Dispatch of requests from the central service to the operative docker containers to benchmark the AI model on the target platform. 
- Check the format and compatibility of the AI model with the available inference engine on the target platform when the model is received at the docker container.
- Benchmark the AI model on the target platform and return the results to the end user through the dispatcher.

## Supported model conversions

Users are not limited to performing benchmarks with the same engine their original models are in. Models can be automatically converted according to the following possibilities:

```
TensorFlow (.h5) -> ONNX (.onnx)
TensorFlow (.h5) -> TensorFlow Lite (.tflite)
ONNX (.onnx) -> TensorRT (.trt)
ONNX (.onnx) -> LPDNN
```

Conversions are transitive, e.g. a TensorFlow model can be converted to LPDNN by a chain of conversions (TensorFlow -> ONNX -> LPDNN).

**Note:** If the LPDNN engine is specified and the automatic export fails (e.g. due to unsupported input shape), the system will fall back to onnxruntime.


## Setting up a Benchmark-as-a-Service docker container

If you already have a Benchmark-as-a-Service docker container running, skip this step.

Pull a docker image from the [BaaS registry](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/). Example:

```
docker pull registry.gitlab.com/bonseyes/benchmark-aas/service/x86_64:_cuda11.5_tensorrt8.2
```

Run the docker container:

```
docker run --name BaaS \
    [--privileged] \
    --rm \
    -it \
    [--gpus all ] \
    -p 8888:8888 \
    <image_name>:<image_tag>
```

Example (with GPU support):

```
docker run --name BaaS --rm -it --gpus all -p 8888:8888 registry.gitlab.com/bonseyes/benchmark-aas/service/x86_64:_cuda11.5_tensorrt8.2
```

Example (CPU-only):

```
docker run --name BaaS --rm -it -p 8888:8888 registry.gitlab.com/bonseyes/benchmark-aas/service/x86_64:_cpu
```

Start a Benchmark-as-a-Service server inside the docker container:

```
user@<container_id>:/app$ python -m bonseyes_baas.process.server -p 8888
```

## BaaS-CLI Installation
Next, clone this repository and move into it:
```
git clone git@gitlab.com:bonseyes/benchmark-aas/service.git BaaS
cd BaaS
```

Now, create and activate a virtual environment, then install the required packages:

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Using the BaaS-CLI tool to communicate with a BaaS server directly

This script allows a user to send an HTTP request directly to the BaaS server (without Dispatcher), based on the given arguments.

**Note:** If needed, adjust the URLs in the baas_map dictionary in the [baas_cli.py](https://gitlab.com/bonseyes/benchmark-aas/service/-/blob/main/baas_cli.py#L11) file.

```
python3 baas_cli.py benchmark \
    --model-path MODEL_PATH \
    --platform {x86_64,jetson,rpi} \
    --profile {cpu,cuda11.4_tensorrt8.0,cuda11.5_tensorrt8.2,jetpack4.6,arm64v8} \
    --engine {tensorflow,onnxruntime,tensorrt,tflite,lpdnn} \
    [--device {gpu,cpu}] \
    [--cpu-num CPU_NUM] \
    [--thread-num THREAD_NUM]
```

**Note:** The profile parameter refers to the tag of the docker image used for deployment. 

For example, on the [x86_64](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154543) platform:
- `cpu` refers to the CPU-only image
- `cuda11.x_tensorrt8.x` profiles refer to the images with GPU support

Similarly, `jetpack4.6` and `arm64v8` are tags for [Jetson](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154631) and [Raspberry Pi](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154529) images, respectively.

Examples:

```
python3 baas_cli.py benchmark \
    --model-path <local_path_to_an_onnx_model> \
    --platform x86_64 \
    --profile cuda11.5_tensorrt8.2 \
    --engine tensorrt \
    --device gpu
```

```
python3 baas_cli.py benchmark \
    --model-path <local_path_to_an_onnx_model> \
    --platform x86_64 \
    --profile cuda11.5_tensorrt8.2 \
    --engine onnxruntime \
    --device cpu \
    --cpu-num 6 \
    --thread-num 12
```


## Using the BaaS-CLI tool to set up a Dispatcher (HTTP server)

The Dispatcher, as shown in the BaaS figure, allows to communicate and forward benchmark requests from users to various BaaS instances, running on different platforms, e.g., X86, Jetson Nvidia, Raspberry Pi.

**Note:** If needed, adjust the URLs in the baas_map dictionary in the [baas_cli.py](https://gitlab.com/bonseyes/benchmark-aas/service/-/blob/main/baas_cli.py#L11) file.

```
python3 baas_cli.py server --port PORT
```


## Using the client script to communicate with the Dispatcher (HTTP server)

Client sends a model in an HTTP request to the dispatcher and then the dispatcher forwards it to a BaaS server running on a specific platform.

```
python3 client.py \
    --url URL \
    --port PORT \
    --model-path MODEL_PATH \
    --platform {x86_64,jetson,rpi} \
    --profile {cpu,cuda11.4_tensorrt8.0,cuda11.5_tensorrt8.2,jetpack4.6,arm64v8} \
    --engine {tensorflow,onnxruntime,tensorrt,tflite,lpdnn} \
    [--device {gpu,cpu}] \
    [--cpu-num CPU_NUM] \
    [--thread-num THREAD_NUM]
```

Examples:

```
python3 client.py \
    --url http://localhost \
    --port 8889 \
    --model-path <local_path_to_an_onnx_model> \
    --platform x86_64 \
    --profile cuda11.5_tensorrt8.2 \
    --engine tensorrt \
    --device gpu
```

```
python3 client.py \
    --url http://localhost \
    --port 8889 \
    --model-path <local_path_to_an_onnx_model> \
    --platform x86_64 \
    --profile cuda11.5_tensorrt8.2 \
    --engine onnxruntime \
    --device cpu \
    --cpu-num 6 \
    --thread-num 12
```
## Using the GUI to communicate with the Dispatcher (HTTP server)

Client can also use the GUI for easy usage of the BaaS. This documentation is designed to help users navigate and utilize the features and functionalities of the BaaS system efficiently.

### Prerequisites

Before executing the GUI, ensure that you have the following prerequisites in place:

- A running Service Server: Ensure that there's at least one service server is up and running to take the benchmark request.
- A running Dispatcher: BaaS GUI relies on the dispatcher server to manage backend services, i.e.,  the GUI does not interact directly with Service server, but instead communicates exclusively through the dispatcher. Ensure that the dispatcher server is up and running before opening the GUI and is well connected to the Service server as explained in the steps above.

***Note:*** In [<u>send.js</u>](https://gitlab.com/bonseyes/benchmark-aas/service/-/blob/main/gui/send.js?ref_type=heads), locate the setting for the dispatcher server port. By default, it is set to port 8889. Change this port number to the port on which your BaaS dispatcher server is running. For example, if your dispatcher server is running on port 9000, update the baas_map in send.js as follows:

````
let base_url = "http://localhost:9000/bonseyes_baas";
````

Servers for different architectures will run on different ports. The URL should be updated alongside the ports if it is not run locally.

### Suported Platforms

The profile parameter refers to the tag of the docker image used for deployment. 

For example, on the [x86_64](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154543) platform:
- `cpu` refers to the CPU-only image
- `cuda11.x_tensorrt8.x` profiles refer to the images with GPU support

Similarly, `jetpack4.6` and `arm64v8` are tags for [Jetson](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154631) and [Raspberry Pi](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154529) images, respectively.

### Usage

1. **Select a Model**:
Start by choosing the model you want to benchmark. This could involve selecting from a list of available models or providing the model's details.


2. **Configure Options**: Customize the benchmarking process by specifying your desired options. These options may include parameters like batch size, input data, or specific benchmarking metrics.


3. **Initiate Benchmarking**: Trigger the benchmarking process, allowing the system to run the chosen model with the configured options.


4. **View Benchmark Results**: 
   * Check the "Eval results" field to preview the benchmark results. This field displays key metrics or statistics related to the model's performance.
   * Charts or graphs illustrating the benchmarking results will be displayed on the right-hand side of the interface. These visualizations can help you interpret the data more easily.
   * If multiple benchmarks are performed, the results will be aggregated. This allows to compare and analyze the performance of different models or configurations.


5. **Clear Results**: It is possible to start fresh or remove the benchmarking data using the "Reset" button.


6. **Download Data**:
   * Benchmark results can be downloaded as both JSON and CSV files. These files can be useful for further analysis or sharing the data.
   * Additionally, charts can also be downloaded as image files. This allows to save and share visual representations of the benchmarking data. 


## Acknowledgments

This service has reused concepts and tools developed during the European Bonseyes and BonsAPPs projects. 
