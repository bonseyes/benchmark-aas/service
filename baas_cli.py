import argparse
import json
import os
import requests
import pandas as pd

from flask import Flask, request, jsonify, redirect
from flask_cors import CORS

from stairwai_utils import generate_model_json


baas_map = {
    "x86_64": {
        # External
        #"cpu": "https://stairwai-baas.cloud.cnaf.infn.it/baas",
        #"cuda11.4_tensorrt8.0": None",
        #"cuda11.5_tensorrt8.2": "https://stairwai-baas.cloud.cnaf.infn.it/baas",
        # Local
        "cpu": "http://localhost:8888/bonseyes_baas",
        "cuda11.4_tensorrt8.0": None,
        "cuda11.5_tensorrt8.2": "http://localhost:8888/bonseyes_baas"
    },
     "jetson": {
         "jetpack4.6": None
     },
     "rpi": {
         "arm64v8": None
     }
}


def cli():
    parser = argparse.ArgumentParser(description="Bonseyes Benchmark-as-a-Service CLI Tool")
    subparser = parser.add_subparsers(help='CLI commands', dest='commands')

    # SERVER
    server_parser = subparser.add_parser('server', help="BaaS HTTP dispatch server.")
    server_parser.add_argument('--port', '-p', required=True, type=int, help='Server port')

    # BENCHMARK
    benchmark_args = subparser.add_parser('benchmark', help="BaaS CLI benchmark.")
    benchmark_args.add_argument(
        '--model-path', 
        '-mo', 
        required=True, 
        type=str, 
        help='Path to model file'
    )
    benchmark_args.add_argument(
        '--platform',
        '-pl',
        required=True,
        type=str,
        choices=['x86_64', 'jetson', 'rpi'],
        help='Choose one or more engines: x86_64 | jetson | rpi',
    )
    benchmark_args.add_argument(
        '--profile',
        '-p',
        required=True,
        type=str,
        choices=[
            'cpu',
            'cuda11.4_tensorrt8.0',
            'cuda11.5_tensorrt8.2',
            'jetpack4.6',
            'arm64v8'
        ],
        help='Available environments: [cpu, cuda10.2_tensorrt7.0, cuda11.2_tensorrt7.2_rtx3070, cuda11.4_tensorrt8.0, cuda11.5_tensorrt8.2, jetpack4.4, jetpack4.6, arm64v8]',
    )
    benchmark_args.add_argument(
        '--engine',
        '-e',
        required=True,
        type=str,
        choices=['tensorflow', 'onnxruntime', 'tensorrt', 'tflite', 'lpdnn'],
        help='Choose one or more engines: tensorflow | onnxruntime | tensorrt | tflite | lpdnn',
    )
    benchmark_args.add_argument(
        '--device',
        '-de',
        required=False,
        default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    benchmark_args.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    benchmark_args.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )

    return parser


app = Flask(__name__)
CORS(app)

@app.route('/bonseyes_baas/<platform>/<profile>/<engine>', methods=['POST'])
def baas_http(platform, profile, engine):
    try:
        base = baas_map[platform][profile]
    except Exception as e:
        for f in os.listdir('tmp'):
            os.remove(os.path.join('tmp', f))
        return f"Platform '{platform}' and Profile '{profile}' do not match", 400

    try:
        if base is None:
            for f in os.listdir('tmp'):
                os.remove(os.path.join('tmp', f))
            return str("Platform currently not available"), 400 

        model = request.files['model']
        filename = model.filename
        model_name, extension = os.path.splitext(filename)
        device = request.args.get('device')
        if device not in ['cpu', 'gpu']:
            device = 'cpu'
        
        if engine not in ['tensorflow', 'onnxruntime', 'tensorrt', 'tflite', 'lpdnn']:
            raise Exception(f'Unknown engine {engine}')
        
        if (
            (extension == '.onnx' and engine in ['tensorflow', 'tflite']) or
            (extension == '.trt' and engine != 'tensorrt') or
            (extension == '.tflite' and engine != 'tflite')
        ):
            raise Exception(f'Cannot convert {extension} model to {engine}')

        model.save(os.path.join('tmp', filename))
        baas_url = f'{base}/{engine}?device={device}'

        ret = requests.post(
            baas_url, 
            files={'model': open(os.path.join('tmp', filename), 'rb')}, 
            timeout=None
        )

        os.remove(os.path.join('tmp', filename))

        return ret.text, ret.status_code

    except Exception as e:
        for f in os.listdir('tmp'):
            os.remove(os.path.join('tmp', f))
        return str(e), 400


def baas_cli(model_path, platform, profile, engine, device, cpu_num, thread_num):
    base = baas_map[platform][profile]

    model_name, extension = os.path.splitext(model_path.split('/')[-1])
    
    if engine not in ['tensorflow', 'onnxruntime', 'tensorrt', 'tflite', 'lpdnn']:
        raise Exception(f'Unknown engine {engine}')
    
    if (
        (extension == '.onnx' and engine in ['tensorflow', 'tflite']) or
        (extension == '.trt' and engine != 'tensorrt') or
        (extension == '.tflite' and engine != 'tflite')
    ):
        raise Exception(f'Cannot convert {extension} model to {engine}')

    baas_url = f'{base}/{engine}?device={device}'
    if cpu_num is not None:
        baas_url += f'&cpu_num={cpu_num}'
    if thread_num is not None:
        baas_url += f'&thread_num={thread_num}'

    ret = requests.post(
        baas_url, 
        files={'model': open(model_path, 'rb')}, 
        timeout=None,
        verify=False
    )

    if ret.status_code == 200:
        
        csv_path = f'eval_results/{model_name}-{platform}.csv'
        results = ret.json()
        results['MODEL'] = model_name
        results['PLATFORM'] = platform
        df = pd.DataFrame([results])
        df.to_csv(csv_path, index=False)

        json_path = f'eval_results/{model_name}-{platform}.json'
        with open(json_path, 'w') as f:
            json.dump(generate_model_json(model_name, platform), f, indent=4)

        print(json.dumps(ret.json(), indent=4))

    else:
        
        print(ret.text)


def main():
    args = cli().parse_args()

    if not os.path.exists('tmp'):
        os.makedirs('tmp')

    # SERVER
    if args.commands == 'server':
        app.run(host='0.0.0.0', port=int(args.port), debug=True)

    # # BENCHMARK
    elif args.commands == 'benchmark':
        baas_cli(
            args.model_path,
            args.platform,
            args.profile,
            args.engine,
            args.device,
            args.cpu_num,
            args.thread_num
        )


if __name__ == '__main__':
    main()
