def generate_model_json(name, platform_name):
    
    model_json = {}
    
    model_json['name'] = name
    model_json['HW_ID'] = platform_name
    model_json['HW_price'] = None
    model_json['hyperparams'] = []
    model_json['targets'] = []

    # Hyperparameters
    field = {}
    field['ID'] = 'VERSION'
    field['description'] = 'Backbone of the model'
    field['type'] = 'str'
    model_json['hyperparams'].append(field)

    field = {}
    field['ID'] = 'INPUT_TILE'
    field['description'] = 'Input size of the model'
    field['type'] = 'str'
    model_json['hyperparams'].append(field)
	
    field = {}
    field['ID'] = 'ENGINE'
    field['description'] = 'Inference engine for model execution'
    field['type'] = 'str'
    model_json['hyperparams'].append(field)
	
    field = {}
    field['ID'] = 'PROCESSSOR'
    field['description'] = 'Processor that executes the model'
    field['type'] = 'str'
    model_json['hyperparams'].append(field)
	
    field = {}
    field['ID'] = 'PRECISION'
    field['description'] = 'Data type precision'
    field['type'] = 'str'
    model_json['hyperparams'].append(field)
	
    field = {}
    field['ID'] = 'GFLOP'
    field['description'] = 'Giga Floating Point Operations to execute the model'
    field['type'] = 'float'
    model_json['hyperparams'].append(field)
	
    field = {}
    field['ID'] = 'PARAMS'
    field['description'] = 'Number of models parameters'
    field['type'] = 'float'
    model_json['hyperparams'].append(field)
	
    field = {}
    field['ID'] = 'STORAGE'
    field['description'] = 'Models storage size in GB'
    field['type'] = 'float'
    model_json['hyperparams'].append(field)
	
    # Targets
    field = {}
    field['ID'] = 'PREPROCESSING_TIME'
    field['description'] = 'Models preprocessing time'
    field['type'] = 'float'
    model_json['targets'].append(field)
	
    field = {}
    field['ID'] = 'INFERENCE_TIME'
    field['description'] = 'Models inference time'
    field['type'] = 'float'
    model_json['targets'].append(field)
    
    field = {}
    field['ID'] = 'POSTPROCESSING_TIME'
    field['description'] = 'Models postprocessing time'
    field['type'] = 'float'
    model_json['targets'].append(field)

    field = {}
    field['ID'] = 'LATENCY'
    field['description'] = 'Models total latency'
    field['type'] = 'float'
    model_json['targets'].append(field)
	
    field = {}
    field['ID'] = 'THROUGHPUT'
    field['description'] = 'Models throughput'
    field['type'] = 'float'
    model_json['targets'].append(field)

    field = {}
    field['ID'] = 'CPU_MEM'
    field['description'] = 'CPU memory during model execution in %'
    field['type'] = 'float'
    model_json['targets'].append(field)

    field = {}
    field['ID'] = 'CPU_MEM_PEAK'
    field['description'] = 'Peak CPU memory during model execution in GB'
    field['type'] = 'float'
    model_json['targets'].append(field)

    field = {}
    field['ID'] = 'GPU_MEM'
    field['description'] = 'GPU memory during model execution in %'
    field['type'] = 'float'
    model_json['targets'].append(field)
	
    field = {}
    field['ID'] = 'GPU_MEM_PEAK'
    field['description'] = 'Peak GPU memory during model execution in GB'
    field['type'] = 'float'
    model_json['targets'].append(field)

    field = {}
    field['ID'] = 'MEMORY_BANDWIDTH'
    field['description'] = 'Memory bandwidth during model execution in GB/s'
    field['type'] = 'float'
    model_json['targets'].append(field)

    field = {}
    field['ID'] = 'CPU_LOAD'
    field['description'] = 'CPU load during model execution in %'
    field['type'] = 'float'
    model_json['targets'].append(field)
	
    field = {}
    field['ID'] = 'GPU_LOAD'
    field['description'] = 'GPU load during model execution in %'
    field['type'] = 'float'
    model_json['targets'].append(field)
	
    field = {}
    field['ID'] = 'NPU_LOAD'
    field['description'] = 'NPU load during model execution in %'
    field['type'] = 'float'
    model_json['targets'].append(field)

    field = {}
    field['ID'] = 'POWER_CONSUMPTION'
    field['description'] = 'Power consumption during model execution in W'
    field['type'] = 'float'
    model_json['targets'].append(field)
	
    field = {}
    field['ID'] = 'ENERGY_EFFICIENCY'
    field['description'] = 'Energy efficiency during model execution in GFLOP/s/W'
    field['type'] = 'float'
    model_json['targets'].append(field)
    
    return model_json

