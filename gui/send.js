let base_url = "http://localhost:8889/bonseyes_baas";

const model_input = document.getElementById('model');
const platform_input = document.getElementById('platform');
const profile_input = document.getElementById('profile');
const engine_input = document.getElementById('engine');
const device_input = document.getElementById('device');
const cpu_num_input = document.getElementById('cpu_num');
const thread_num_input = document.getElementById('thread_num');
const submit_button = document.getElementById("submit");
const export_json_button = document.getElementById('export_json');
const export_csv_button = document.getElementById('export_csv');

const textarea = document.getElementById('output');
const reset_button = document.getElementById('reset')

const overlay = document.getElementById('overlay');
const spinner = document.getElementById('spinner');

// Model name and platform are used as JSON/CSV filenames
let _model;
let _platform;

// Evaluation JSON object which is used when drawing graphs
let eval_json = [];

document.getElementById('sub_form').addEventListener('submit', function(event) {
    event.preventDefault();

    let fileInput = document.getElementById('model');
    let file = fileInput.files[0];

    let platform = platform_input.value;
    let profile = profile_input.value;
    let engine = engine_input.value;
    let device = device_input.value;
    let cpu_num = cpu_num_input.value;
    let thread_num = thread_num_input.value;

    let base_filename = file.name;
    let temp = base_filename.split('.');
    let extension = temp.pop();

    _model = base_filename;
    _platform = platform;

    if(!checkExtensionValidity(extension)){
        Swal.fire({
            icon: 'error',
            title: 'Unsupported file',
            text: `Accepted input formats are: h5, onnx, trt, and tflite!`
        });
        return;
    }

    if (!checkModelConversion(extension, engine)){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: `Cannot convert .${extension} model to ${engine}!`
        });
        return;
    }

    disableForm();
    showSpinner();

    let baas_url = `${base_url}/${platform}/${profile}/${engine}?device=${device}`;

    // Check if optional params are present
    if(cpu_num.trim() !== '')
        baas_url = baas_url + `&cpu_num=${cpu_num}`;
    if(thread_num.trim() !== '')
        baas_url = baas_url + `&thread_num=${thread_num}`;

    const formData = new FormData();
    formData.append('model', file);

    fetch(baas_url, {
        method : "POST",
        body : formData
    })
        .then(res => {
            if (res.ok) {
                return res.json();
            } else {
                return Promise.all([res.status, res.text()]).then(([status, errorMessage]) => {
                    throw {
                        status,
                        message: errorMessage
                    };
                });
            }
        })
        .then(data => {

            let temp_json = data;
            temp_json['NAME'] = _model;

            eval_json.push(temp_json);

            textarea.value = JSON.stringify(eval_json, null, 2);
            textarea.dispatchEvent(new Event('change', { bubbles: true }));

            updateGraph(temp_json);

            enableForm();
            hideSpinner();
        })
        .catch(err => {
            console.log(err)
            Swal.fire({
                icon: 'error',
                title: `Error ${err.status}`,
                text: `${err.message}`
            });
            enableForm();
            hideSpinner();
        });

});

document.getElementById('export_json').addEventListener("click", function (event){
    let text = textarea.value;
    let filename = `eval_results.json`;

    saveStringAsFile(text, filename);

});

document.getElementById('export_csv').addEventListener("click", function (event){
    let input = JSON.parse(document.getElementById('output').value);
    let text = convertJsonToCsv(input);
    let filename = `eval_results.csv`;

    saveStringAsFile(text, filename);

});
textarea.addEventListener('change', function() {
    if (textarea.value.trim() === '') {
        export_json_button.disabled = true;
        export_csv_button.disabled = true;
    } else {
        export_json_button.disabled = false;
        export_csv_button.disabled = false;
    }
});

function saveStringAsFile(text, filename) {
    const blob = new Blob([text], { type: 'application/json' });
    const url = URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.download = filename;
    link.click();

    URL.revokeObjectURL(url);
}

function disableForm() {
    model_input.disabled = true;
    platform_input.disabled = true;
    profile_input.disabled = true;
    engine_input.disabled = true;
    device_input.disabled = true;
    cpu_num_input.disabled = true;
    thread_num_input.disabled = true;
    export_json_button.disabled = true;
    export_csv_button.disabled = true;
    submit_button.disabled = true;
    reset_button.disabled = true;
    overlay.style.display = 'block';
}

function enableForm() {
    model_input.disabled = false;
    platform_input.disabled = false;
    profile_input.disabled = false;
    engine_input.disabled = false;
    device_input.disabled = false;
    cpu_num_input.disabled = false;
    thread_num_input.disabled = false;
    export_json_button.disabled = false;
    export_csv_button.disabled = false;
    submit_button.disabled = false;
    reset_button.disabled = false;
    overlay.style.display = 'none';
}

function showSpinner() {
    spinner.style.display = 'block';
}

function hideSpinner() {
    spinner.style.display = 'none';
}

function convertJsonToCsv(jsonData) {
    const separator = ',';
    let csv = '';

    const keys = Object.keys(jsonData[0]);
    csv += keys.join(separator) + '\n';

    jsonData.forEach(function (eval, index)
    {
        const keys = Object.keys(eval);
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            const value = eval[key];
            csv += `${value},`
        }
        csv += '\n';
    });

    return csv;
}

function checkModelConversion(extension, engine){
    return !((extension === 'onnx' && (engine === 'tensorflow' || engine === 'tflite')) ||
        (extension === 'trt' && engine !== 'tensorrt') ||
        (extension === 'tflite' && engine !== 'tflite'));
}

function checkExtensionValidity(extension){
    return (extension === "trt" || extension === "h5" || extension === "onnx" || extension === "tflite")
}