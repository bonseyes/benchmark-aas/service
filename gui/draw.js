// Graph colors

const COLORS = [
    'rgb(77, 201, 246)',
    'rgb(246, 112, 25)',
    'rgb(245, 55, 148)',
    'rgb(83, 123, 196)',
    'rgb(172, 194, 54)',
    'rgb(22, 106, 143)',
    'rgb(0, 169, 80)',
    'rgb(88, 89, 91)',
    'rgb(133, 73, 186)'
];

const TRANSPARENT_COLORS = [
    'rgb(77, 201, 246, 0.5)',
    'rgb(246, 112, 25, 0.5)',
    'rgb(245, 55, 148, 0.5)',
    'rgb(83, 123, 196, 0.5)',
    'rgb(172, 194, 54, 0.5)',
    'rgb(22, 106, 143, 0.5)',
    'rgb(0, 169, 80, 0.5)',
    'rgb(88, 89, 91, 0.5)',
    'rgb(133, 73, 186, 0.5)'
];

function color(index, isTransparent) {
    if (isTransparent)
        return TRANSPARENT_COLORS[index % TRANSPARENT_COLORS.length];
    else
        return COLORS[index % COLORS.length];
}

// Counter used for changing colors for every new entry
let numberOfEntries = 0;

// Get the canvas element
const ctx = document.getElementById('myChart').getContext('2d');
const ctx2 = document.getElementById('myChart2').getContext('2d');
const ctx3 = document.getElementById('myChart3').getContext('2d');
const ctx4 = document.getElementById('myChart4').getContext('2d');
const ctx5 = document.getElementById('myChart5').getContext('2d');
const ctx6 = document.getElementById('myChart6').getContext('2d');


// Data property of an empty chart
const baseConfigData = {
    LATENCY: {
        labels: ["Latency"],
        datasets: []
    },
    CPU_LOAD: {
        labels: ["CPU load"],
        datasets: []
    },
    CPU_MEM_PEAK: {
        labels: ["CPU mem peak"],
        datasets: []
    },
    GPU_LOAD: {
        labels: ["GPU load"],
        datasets: []
    },
    GPU_MEM_PEAK: {
        labels: ["GPU mem peak"],
        datasets: []
    },
    STORAGE: {
        labels: ["Storage"],
        datasets: []
    }
};

// Function that generates graph config
function generateConfig(configData) {
    let value;
    if(configData['labels'][0] === 'CPU mem peak' || configData['labels'][0] === 'GPU mem peak')
        value = 'GB';
    else if (configData['labels'][0] === 'CPU load' || configData['labels'][0] === 'GPU load')
        value = '%';
    else if(configData['labels'][0] === 'Storage')
        value = 'B'
    else if(configData['labels'][0] === 'Latency')
        value = 'ms'


    return {
        type: 'bar',
        data: configData,
        options: {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: `Values (${value})`
                    }
                }
            },
            plugins: {
                legend: {
                    display: true,
                    position: 'bottom',
                }
            }
        },
    };
}

// Generate graph config for every param that we want to graph, and create two graphs that are in default view
let configs = {};
const data_keys = Object.keys(baseConfigData);
data_keys.forEach(function (data_key) {
    configs[data_key] = JSON.parse(JSON.stringify(generateConfig(baseConfigData[data_key])));
})

var myChart = new Chart(ctx, configs['CPU_LOAD']);
var myChart2 = new Chart(ctx2, configs['CPU_MEM_PEAK']);
var myChart3 = new Chart(ctx3, configs['LATENCY']);
var myChart4 = new Chart(ctx4, configs['STORAGE']);
var myChart5 = new Chart(ctx5, configs['GPU_LOAD']);
var myChart6 = new Chart(ctx6, configs['GPU_MEM_PEAK']);

// Updates graph for every new entry
function updateGraph(params) {
    data_keys.forEach(function (dataKey) {
        let currentData = {
            datasets: [
                {
                    label: params['NAME'] + " " + params['PLATFORM'] + " " + params['ENGINE'] + " " + params["PROCESSOR"],
                    data: [params[dataKey]],
                    borderColor: color(numberOfEntries, false),
                    backgroundColor: color(numberOfEntries, true),
                    borderWidth: 1
                }
            ]
        }

        configs[dataKey].data.datasets.push(currentData.datasets[0]);
        myChart.update();
        myChart2.update();
        myChart3.update();
        myChart4.update();
        myChart5.update();
        myChart6.update();
    });
    numberOfEntries++;
}

// Resets eval result field and graphs
reset_button.addEventListener("click", function (event){
    textarea.value = '';
    eval_json = [];

    data_keys.forEach(function (dataKey) {
        configs[dataKey].data.datasets = [];
    });

    myChart.update();
    myChart2.update();
    myChart3.update();
    myChart4.update();
    myChart5.update();
    myChart6.update();

    numberOfEntries = 0;

})

document.getElementById("download").addEventListener("click", function () {
    downloadGraphs();
});

// Draws all graphs on one image and downloads it as PNG
function downloadGraphs(){

    const mergedCanvas = document.createElement('canvas');
    const mergedContext = mergedCanvas.getContext('2d');

    let graphWidth = Math.max(ctx.canvas.width, ctx2.canvas.width, ctx3.canvas.width, ctx4.canvas.width, ctx5.canvas.width, ctx6.canvas.width);
    let graphHeight = Math.max(ctx.canvas.height, ctx2.canvas.height, ctx3.canvas.height, ctx4.canvas.height, ctx5.canvas.height, ctx6.canvas.height);

    const targetWidth = graphWidth * 3 + 4 * 20;
    const targetHeight = graphHeight * 2 + 3 * 30;

    const horizontalSpacing = 20;
    const verticalSpacing = 30;

    mergedCanvas.width = targetWidth;
    mergedCanvas.height = targetHeight;

    mergedContext.fillStyle = '#ffffff';
    mergedContext.fillRect(0, 0, mergedCanvas.width, mergedCanvas.height);

    let x = horizontalSpacing;
    let y = verticalSpacing;
    mergedContext.drawImage(ctx.canvas, x, y, graphWidth, graphHeight);
    x += graphWidth + horizontalSpacing;
    mergedContext.drawImage(ctx2.canvas, x, y, graphWidth, graphHeight);
    x += graphWidth + horizontalSpacing;
    mergedContext.drawImage(ctx3.canvas, x, y, graphWidth, graphHeight);
    x = horizontalSpacing;
    y += graphHeight + verticalSpacing;
    mergedContext.drawImage(ctx4.canvas, x, y, graphWidth, graphHeight);
    x += graphWidth + horizontalSpacing;
    mergedContext.drawImage(ctx5.canvas, x, y, graphWidth, graphHeight);
    x += graphWidth + horizontalSpacing;
    mergedContext.drawImage(ctx6.canvas, x, y, graphWidth, graphHeight);
    x += graphWidth + horizontalSpacing;

    mergedCanvas.toBlob(function (blob) {
        const link = document.createElement('a');
        link.href = URL.createObjectURL(blob);
        link.download = 'all_graphs.png';
        link.click();
        URL.revokeObjectURL(link.href);
    }, 'image/png');
}
