# Baas GUI

## Description

---

The BaaS (Benchmark as a Service) GUI Documentation provides comprehensive information and guidance on the graphical user interface (GUI) used to interact with the BaaS platform. This documentation is designed to help users navigate and utilize the features and functionalities of the BaaS system efficiently.

## Prerequisites

---

Before installing BaaS GUI, ensure that you have the following prerequisites in place:

A working dispatcher server: BaaS GUI relies on a dispatcher server to manage backend services. Ensure that the dispatcher server is up and running before proceeding with the installation. BaaS GUI does not interact directly with Service server, but instead communicates exclusively through the dispatcher.

***Note:*** In [<u>send.js</u>](https://gitlab.com/bonseyes/benchmark-aas/service/-/blob/main/gui/send.js?ref_type=heads), locate the setting for the dispatcher server port. By default, it is set to port 8889. Change this port number to the port on which your BaaS dispatcher server is running. For example, if your dispatcher server is running on port 9000, update the baas_map in send.js as follows:

````
let base_url = "http://localhost:9000/bonseyes_baas";
````

Servers for different architectures will run on different ports. The URL should be updated alongside the ports if it is not run locally.


## Suported Platforms

The profile parameter refers to the tag of the docker image used for deployment. 

For example, on the [x86_64](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154543) platform:
- `cpu` refers to the CPU-only image
- `cuda11.x_tensorrt8.x` profiles refer to the images with GPU support

Similarly, `jetpack4.6` and `arm64v8` are tags for [Jetson](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154631) and [Raspberry Pi](https://gitlab.com/bonseyes/benchmark-aas/service/container_registry/4154529) images, respectively.

## Usage

---

1. **Select a Model**:
Start by choosing the model you want to benchmark. This could involve selecting from a list of available models or providing the model's details.


2. **Configure Options**: Customize the benchmarking process by specifying your desired options. These options may include parameters like batch size, input data, or specific benchmarking metrics.


3. **Initiate Benchmarking**: Trigger the benchmarking process, allowing the system to run the chosen model with the configured options.


4. **View Benchmark Results**: 
   * Check the "Eval results" field to preview the benchmark results. This field displays key metrics or statistics related to the model's performance.
   * Charts or graphs illustrating the benchmarking results will be displayed on the right-hand side of the interface. These visualizations can help you interpret the data more easily.
   * If you perform multiple benchmarks, the results will be aggregated. This means that you can compare and analyze the performance of different models or configurations.


5. **Clear Results**: If you want to start fresh or remove the benchmarking data, you can use the "Reset" button to clear the results.


6. **Download Data**:
   * You have the option to download the benchmark results as both JSON and CSV files. These files can be useful for further analysis or sharing the data.
   * Additionally, you can download the charts as image files. This allows you to save and share visual representations of the benchmarking data. 
